package net.sf.freecol.common.util;

import java.io.File;
import java.util.Objects;

public class ReaderFromResources {

    public static File getFileFromResource(String fileName) {
        return new File(Objects.requireNonNull(ReaderFromResources.class.getClassLoader().getResource(fileName)).getFile());
    }

    public static File getFileFromResource(String fileName, String child) {
        return new File(Objects.requireNonNull(ReaderFromResources.class.getClassLoader().getResource(fileName)).getFile(), child);
    }
}